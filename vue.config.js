const StyleLintPlugin = require("stylelint-webpack-plugin");
const MonacoEditorPlugin = require("monaco-editor-webpack-plugin");
const path = require("path");
const { name } = require('./package')

module.exports = {
    devServer: {
        port: 8888,
        // proxy: {
        
          
        // },
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    },
    // publicPath: "/freeeditor/",
    publicPath: './',
    productionSourceMap: false,
    lintOnSave: false,
    css: {
        loaderOptions: {
            sass: {
                prependData: `
          @import "~@/assets/styles/variable.scss";
          @import "~@/assets/styles/mixin.scss";
        `,
            },
            less: {
                lessOptions: {
                    modifyVars: {
                        "primary-color": "#d14424",
                        "text-color": "#41464b",
                        "font-size-base": "13px",
                        "border-radius-base": "2px",
                    },
                    javascriptEnabled: true,
                },
            },
        },
    },
    configureWebpack: {
        // devtool: "source-map",
        // entry: {
        //     // app: './index.js',
        //     // Package each language's worker and give these filenames in `getWorkerUrl`
        //     "editor.worker": "monaco-editor/esm/vs/editor/editor.worker.js",
        //     "json.worker": "monaco-editor/esm/vs/language/json/json.worker",
        //     "css.worker": "monaco-editor/esm/vs/language/css/css.worker",
        //     "html.worker": "monaco-editor/esm/vs/language/html/html.worker",
        //     "ts.worker": "monaco-editor/esm/vs/language/typescript/ts.worker",
        // },
        output: {
            library: `${name}-[name]`,
            libraryTarget: 'umd', // 把子应用打包成 umd 库格式
            jsonpFunction: `webpackJsonp_${name}`
        },
        // output: {
        //     globalObject: 'self',
        //     filename: '[name].bundle.js',
        //     path: path.resolve(__dirname, 'dist')
        // },
        // module: {
        //     rules: [
        //         {
        //             test: /\.css$/,
        //             use: ['style-loader', 'css-loader']
        //         },
        //         {
        //             test: /\.ttf$/,
        //             use: ['file-loader']
        //         }
        //     ]
        // },
        plugins: [
            new StyleLintPlugin({
                files: ["src/**/*.{vue,html,css,scss,sass,less}"],
                failOnError: false,
                cache: false,
                fix: false,
            }),
            new MonacoEditorPlugin({
                // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
                // Include a subset of languages support
                // Some language extensions like typescript are so huge that may impact build performance
                // e.g. Build full languages support with webpack 4.0 takes over 80 seconds
                // Languages are loaded on demand at runtime
                languages: ["javascript", "css", "html", "typescript", "json"],
            }),
        ],
    },
};