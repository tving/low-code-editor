import { SlideTheme } from '@/types/slides'
import { SYS_FONTS } from '@/configs/font'
import { isSupportFont } from '@/utils/font'

export const theme: SlideTheme = {
  themeColor: '#454CE1',
  fontColor: '#413A50',
  fontName: SYS_FONTS.filter(font => isSupportFont(font.value))[0].label,
  backgroundColor: '#ffffff',
}