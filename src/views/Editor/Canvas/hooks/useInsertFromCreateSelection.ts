import { Ref } from 'vue'
import { storeToRefs } from 'pinia'
import { useMainStore } from '@/store'
import { CreateElementSelectionData, CreatingLineElement, CreatingShapeElement } from '@/types/edit'
import useCreateElement from '@/hooks/useCreateElement'
import { ChartType, TextType } from '@/types/slides'

export default (viewportRef: Ref<HTMLElement | undefined>) => {
  const mainStore = useMainStore()
  const { canvasScale, creatingElement, canvasHeight, canvasWidth } = storeToRefs(mainStore)

  // 通过鼠标框选时的起点和终点，计算选区的位置大小
  const formatCreateSelection = (selectionData: CreateElementSelectionData) => {
    const { start, end } = selectionData

    if (!viewportRef.value) return
    const viewportRect = viewportRef.value.getBoundingClientRect()
    const [startX, startY] = start
    const [endX, endY] = end
    const minX = Math.min(startX, endX)
    const maxX = Math.max(startX, endX)
    const minY = Math.min(startY, endY)
    const maxY = Math.max(startY, endY)

    let left = (minX - viewportRect.x) / canvasScale.value
    let top = (minY - viewportRect.y) / canvasScale.value
    const width = (maxX - minX) / canvasScale.value
    const height = (maxY - minY) / canvasScale.value
    console.log(left, canvasWidth.value * canvasScale.value, 'viewportRect')
    // 处理元素拖拽到边缘 元素整个到画布里
    if (canvasScale.value > 1) {
      left > (canvasWidth.value * canvasScale.value) - width / 2 ? left = (minX - viewportRect.x) / canvasScale.value - width / 2 : ''
      left < width / 2 ? left = width / 2 : ''
      top > (canvasHeight.value * canvasScale.value) - height / 2 ? top = (minY - viewportRect.y) / canvasScale.value - height / 2 : ''
      top < height / 2 ? top = width / 2 : ''
    }

    return { left, top, width, height }
  }

  // 通过鼠标框选时的起点和终点，计算线条在画布中的位置和起点终点
  const formatCreateSelectionForLine = (selectionData: CreateElementSelectionData) => {
    const { start, end } = selectionData

    if (!viewportRef.value) return
    const viewportRect = viewportRef.value.getBoundingClientRect()

    const [startX, startY] = start
    const [endX, endY] = end
    const minX = Math.min(startX, endX)
    const maxX = Math.max(startX, endX)
    const minY = Math.min(startY, endY)
    const maxY = Math.max(startY, endY)

    let left = (minX - viewportRect.x) / canvasScale.value
    let top = (minY - viewportRect.y) / canvasScale.value
    const width = (maxX - minX) / canvasScale.value
    const height = (maxY - minY) / canvasScale.value
    console.log(width, height)

    // 处理元素拖拽到边缘 元素整个到画布里
    if (canvasScale.value > 1) {
      left > (canvasWidth.value * canvasScale.value) - width / 2 ? left = (minX - viewportRect.x) / canvasScale.value - width / 2 : ''
      left < width / 2 ? left = width / 2 : ''
      top > (canvasHeight.value * canvasScale.value) - height / 2 ? top = (minY - viewportRect.y) / canvasScale.value - height / 2 : ''
      top < height / 2 ? top = height / 2 : ''
    }

    const _start: [number, number] = [
      startX === minX ? 0 : width,
      startY === minY ? 0 : height,
    ]
    const _end: [number, number] = [
      endX === minX ? 0 : width,
      endY === minY ? 0 : height,
    ]

    return {
      left,
      top,
      start: _start,
      end: _end,
    }
  }

  const { createTextElement, createShapeElement, createLineElement, createChartElement, createImageElement, createCustomElement, createVideoElement } = useCreateElement()

  // 根据鼠标选区的位置大小插入元素
  const insertElementFromCreateSelection = (selectionData: CreateElementSelectionData) => {
    if (mainStore.dragIndex === '') {
      mainStore.setCreatingElement(null)
      return
    }
    console.log(creatingElement.value, 'creatingElement')
    if (!creatingElement.value) return

    const type = creatingElement.value.type
    if (type === 'text') {
      const position = formatCreateSelection(selectionData)
      position && createTextElement(position, creatingElement.value.data)
    }
    else if (type === 'shape') {
      const position = formatCreateSelection(selectionData)
      position && createShapeElement(position, (creatingElement.value as CreatingShapeElement).data)
    }
    else if (type === 'line') {
      const position = formatCreateSelectionForLine(selectionData)
      position && createLineElement(position, (creatingElement.value as CreatingLineElement).data)
    }
    else if (type === 'chart') {
      const position = formatCreateSelectionForLine(selectionData)
      console.log(position,'-------')

      position && createChartElement(position, creatingElement.value.data, creatingElement.value.antvdata)
    }
    else if (type === 'image') {
      const position = formatCreateSelection(selectionData)
      position && createImageElement(position, creatingElement.value.data, creatingElement.value.size)
    }
    else if (type === 'custom') {
      const position = formatCreateSelection(selectionData)
      position && createCustomElement(position, creatingElement.value.data)
    }
    else if (type === 'video') {
      const position = formatCreateSelection(selectionData)
      position && createVideoElement(position, creatingElement.value.data, creatingElement.value)
    }
    mainStore.setDragIndex('')
    mainStore.setCreatingElement(null)
  }

  return {
    insertElementFromCreateSelection,
  }
}
