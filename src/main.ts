import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createRouter, createWebHistory, createWebHashHistory, createMemoryHistory } from 'vue-router';
import { SketchRule } from 'vue3-sketch-ruler'

import App from './App.vue'
import routes from './routers/index'
import '@icon-park/vue-next/styles/index.css'
import 'prosemirror-view/style/prosemirror.css'
import 'animate.css'
import Antd from 'ant-design-vue/es'
import 'ant-design-vue/dist/antd.css'

import '@/assets/styles/prosemirror.scss'
import '@/assets/styles/global.scss'
import '@/assets/styles/antd.scss'
import '@/assets/styles/font.scss'
import '@/assets/styles/index.scss'

import '@/assets/iconfont/iconfont.css'
import '@/assets/iconfont/iconfont.js'
// 引入标尺
import 'vue3-sketch-ruler/lib/style.css'

import Icon from '@/plugins/icon'
import Component from '@/plugins/component'
import Directive from '@/plugins/directive'

import VueLazyload from 'vue-lazyload'

//@ts-nocheck
import './public-path';

import {
  InputNumber,
  Divider,
  Button,
  Tooltip,
  Popover,
  Slider,
  Select,
  Switch,
  Radio,
  Input,
  Modal,
  Dropdown,
  Menu,
  Checkbox,
  Drawer,
  Spin,
  Alert,
} from 'ant-design-vue'

let router = null;
let instance: any = null;
let history: any = null;
const win: any = window;

function render(props: any) {
  const container: any = props.container;
  if(win.__POWERED_BY_QIANKUN__) {
    history = createMemoryHistory(`/freeeditor`);
  } else {
    history = createWebHashHistory('/');
  }
  
  router = createRouter({
    history,
    routes,
  });

  instance = createApp(App);
  instance.component('InputNumber', InputNumber)
  instance.component('Divider', Divider)
  instance.component('Button', Button)
  instance.component('ButtonGroup', Button.Group)
  instance.component('Tooltip', Tooltip)
  instance.component('Popover', Popover)
  instance.component('Slider', Slider)
  instance.component('Select', Select)
  instance.component('SelectOption', Select.Option)
  instance.component('SelectOptGroup', Select.OptGroup)
  instance.component('Switch', Switch)
  instance.component('Radio', Radio)
  instance.component('RadioGroup', Radio.Group)
  instance.component('RadioButton', Radio.Button)
  instance.component('Input', Input)
  instance.component('InputGroup', Input.Group)
  instance.component('TextArea', Input.TextArea)
  instance.component('Modal', Modal)
  instance.component('Dropdown', Dropdown)
  instance.component('Menu', Menu)
  instance.component('MenuItem', Menu.Item)
  instance.component('Checkbox', Checkbox)
  instance.component('Drawer', Drawer)
  instance.component('Spin', Spin)
  instance.component('Alert', Alert)
  instance.component('SketchRule', SketchRule)

  instance.use(Icon)
  instance.use(Component)
  instance.use(Directive)
  instance.use(Antd)

  instance.use(router)

  instance.use(VueLazyload, {
    preLoad: 1.3,
    error: require('@/assets/images/card_default.png'),
    loading: require('@/assets/images/card_default.png'),
    attempt: 1
  })

  instance.use(createPinia())

  instance.mount(container ? container.querySelector('#freeeditor-app') : '#freeeditor-app');
  if (win.__POWERED_BY_QIANKUN__) {
    console.log('我正在作为子应用运行')
  }
}

if (!win.__POWERED_BY_QIANKUN__) {
  render({});
}

export async function bootstrap() {
  console.log('bootstrap');
}

export async function mount(props: any) {
  console.log("viteapp mount");

  render(props);
}

export async function unmount() {
  instance.unmount();
  instance._container.innerHTML = '';
  instance = null;
  router = null;
  history.destroy();
}



