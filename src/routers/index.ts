import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
// import { config } from '@/config'
// import { getToken } from '@/utils/token-util'
// import { UserStore } from '@/domains/user'
import Home from '@/views/workspace/index.vue'

// const navRoutes: Array<RouteRecordRaw> = [

// ]

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'freeeditor',
    component: () => import('@/views/freeeditor/index.vue'),
    meta: { title: 'freeeditor' },
  },
  // {
  //   path: '/',
  //   name: 'workspace',

  //   component: Home,
  //   meta: { title: 'workspace' },
  // },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/Login/index.vue"),
    meta: { title: "login" },
  },
  // {
  //   path: '/:catchAll(.*)*',
  //   name: 'NotFound',
  //   component: () => import('@/views/error/index.vue'),
  //   meta: { title: '找不到页面' },
  // },
]



const router = createRouter({
  history: createWebHashHistory(),
  routes,
})


// const whiteList = ['/login', '/auth-redirect', '/dev'] // no redirect whitelist

// router.beforeEach(async (to, from, next) => {
//   // set page title
//   if (to.meta && to.meta.title) {
//     document.title = `${to.meta.title} | ${config.title}`
//   } else {
//     document.title = config.title
//   }

//   const hasToken = getToken()
//   if (hasToken) {
//     if (to.path === '/login') {
//       next({ path: '/' })
//     } else {
//       const { role, getUserInfo, resetToken } = UserStore()
//       if (role.value > 0) {
//         next()
//       } else {
//         try {
//           await getUserInfo()
//           next({ ...to, replace: true })
//         } catch (error) {
//           resetToken()
//           next(`/login?redirect=${to.path}`)
//         }
//       }
//     }
//   } else if (whiteList.some(m => to.path.startsWith(m))) {
//     next()
//   } else {
//     next(`/login?redirect=${to.path}`)
//   }
// })


export default routes
