import { LinePoint } from '@/types/slides'

export const TEXT_LIST = [
    {   
        name: '标题',
        icon: 'appIcon-wenzi-biaotimoren',
        width: 420,
        height: 77,
        content: '<p style=\'\'><strong><span style=\'font-size: 48px\'>在此处添加标题</span></strong></p>',
    },
    {   
        name: '字幕',
        icon: 'appIcon-wenzi-zimumoren',
        width: 240,
        height: 56,
        content: '<p style=\'\'><span style=\'font-size: 24px\'>在此处添加副标题</span></p>',
    },
    {
        name: '文章主题',
        icon: 'appIcon-wenzi-wenzhangzhutimoren',
        width: 420,
        height: 340,
        content: "<p><span style='font-size: 24px'>在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容在此处输入内容</span></p>",
    },
    {
        name: '说明文字',
        icon: 'appIcon-wenzi-shuomingwenzimoren',
        width: 385,
        height: 50,
        content: '<p style=""><span style="color: rgba(104,103,103,1);"><span style="font-size: 20px">在此处添加说明文字</span></span></p>',
    },
    {
        name: '渐变文字',
        icon: 'appIcon-wenzi-shuomingwenzimoren',
        width: 205,
        height: 50,
        content: '<p class="linear"><span style=""><span style="background: linear-gradient(120deg, #30cfd0 0%, #ebbba7 50%, #a3bded 80%);">在此处添加说明文字</span></span></p>',
    },
    {
        name: '滚动文字',
        icon: 'appIcon-wenzi-wenzhangzhutimoren',
        width: 420,
        height: 340,
        content: '<p style="text-align: center;"><span style="font-size: 24px">在此处输入内容</span></p><p style="text-align: center;"><span style="font-size: 24px">在此处输入内容</span></p><p style="text-align: center;"><span style="font-size: 24px">在此处输入内容</span></p><p style="text-align: center;"><span style="font-size: 24px">在此处输入内容</span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p><p style="text-align: center;"><span style="color: rgba(65,58,80,1);"><span style="font-size: 24px">在此处输入内容</span></span></p>',
    },
    
    
    
    
]