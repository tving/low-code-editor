export const PROJECTDATA = [
  {
    name: "信息图",
    type: "folder",
    width: 700,
    height: 1500,
    w: 144,
    h: 170,
    icon: "appIcon-xinxitubeijing",
    icon1: "appIcon-xinxitu1",
    percent: 100,
  },
  {
    name: "报告",
    type: "folder",
    width: 600,
    height: 850,
    w: 144,
    h: 160,
    icon: "appIcon-baogaobeijing",
    icon1: "appIcon-baogao2",
    percent: 100,
  },
  {
    name: "幻灯片",
    type: "folder",
    width: 1600,
    height: 900,
    w: 144,
    h: 110,
    icon: "appIcon-huandengpian",
    icon1: "appIcon-huandengpian3",
    percent: 75,
  },
  {
    name: "指示板",
    type: "folder",
    width: 1500,
    height: 800,
    w: 144,
    h: 110,
    icon: "appIcon-zhishiban",
    icon1: "appIcon-zhishiban1",
    percent: 75,
  },
  {
    name: "社交媒体",
    type: "folder",
    width: 1080,
    height: 1080,
    w: 144,
    h: 144,
    icon: "appIcon-shejiaomeitibeijing",
    icon1: "appIcon-shejiaomeiti2",
    percent: 100,
  },
  {
    name: "海报",
    type: "folder",
    width: 1080,
    height: 1080,
    w: 144,
    h: 160,
    icon: "appIcon-haibao",
    icon1: "appIcon-haibao1",
    percent: 100,
  },
  {
    name: "手机布局",
    type: "folder",
    width: 414,
    height: 896,
    w: 144,
    h: 150,
    icon: "appIcon-shoujibujubeijing",
    icon1: "appIcon-shoujibuju",
    percent: 100,
  },
  // {
  //   name: "上传文件",
  //   type: "folder",
  //   count: 12,
  //   icon: "appIcon-shangchuanwenjian1",
  // },
]
export const VIEWPOT = {
  '2.14': {
    name: '信息图',
    width: 700,
    height: 1500
  },
  '1.42': {
    name: '报告',
    width: 12,
    height: 17
  },
  '0.56': {
    name: '幻灯片',
    width: 16,
    height: 9
  },
  '0.53': {
    name: '指示板',
    width: 15,
    height: 8
  },
  '1.00': {
    name: '海报',
    width: 1080,
    height: 1080
  },
  '2.16': {
    name: '手机端',
    width: 414,
    height: 896
  },
}
//  信息图 7:22
//   报告   12：17
//   幻灯片  16：9
//   指示板  15：8
//   社交媒体 1：1
//   海报1：1
//   手机 414：896 