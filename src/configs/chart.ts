// const CHART_LIST = [
//     {
//         name: '柱状图',
//         icon: 'icon-jibenzhuzi',
//         width: 400,
//         height: 77,
//         type: 'bar',
//         content: '<p style=\'\'><strong><span style=\'font-size: 48px\'>在此处添加标题</span></strong></p>',
//     },
//     {
//         name: '折线图',
//         icon: 'icon-jibenzhexian',
//         width: 240,
//         height: 56,
//         type: 'line',
//         content: '<p style=\'\'><span style=\'font-size: 24px\'>在此处添加副标题</span></p>',
//     },
//     {
//         name: '饼状图',
//         icon: 'icon-bingzhuangtu',
//         width: 420,
//         height: 340,
//         type: 'pie',
//         content: "<p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p>",
//     }

// ]

// export const BAR_CHART_LIST = [
//     {
//         name: '横向对比图',
//         icon: 'icon-jibenzhuzi',
//         width: 400,
//         height: 77,
//         type: 'bar',
//         content: '<p style=\'\'><strong><span style=\'font-size: 48px\'>在此处添加标题</span></strong></p>',
//     },
//     {
//         name: '气球柱状图',
//         icon: 'icon-jibenzhexian',
//         width: 240,
//         height: 56,
//         type: 'line',
//         content: '<p style=\'\'><span style=\'font-size: 24px\'>在此处添加副标题</span></p>',
//     },
//     {
//         name: '基础柱状图',
//         icon: 'icon-bingzhuangtu',
//         width: 420,
//         height: 340,
//         type: 'pie',
//         content: "<p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p>",
//     }

// ]

// 柱状图
const HISTOGRAM = [
	{
		name: '基础柱状图',
		antVData: {
			type: {
				s: 'name',
				x: 'time', //x轴
				y: 'value',//y轴
				adjust: true,//分组
				position: "time*value",//x*y
				xLine: true, //是否显示横轴辅助线
			},
			data: [
				{ "time": "AA", "value": 235, "name": "系列一" },
				{ "time": "BB", "value": 200, "name": "系列一" },
				{ "time": "CC", "value": 25, "name": "系列一" },
				{ "time": "DD", "value": 190, "name": "系列一" },
				{ "time": "EE", "value": 90, "name": "系列一" },
				{ "time": "FF", "value": 142, "name": "系列一" },
				{ "time": "GG", "value": 50, "name": "系列一" },
				{ "time": "HH", "value": 250, "name": "系列一" },
				{ "time": "AA", "value": 20, "name": "系列二" },
				{ "time": "BB", "value": 20, "name": "系列二" },
				{ "time": "CC", "value": 120, "name": "系列二" },
				{ "time": "DD", "value": 30, "name": "系列二" },
				{ "time": "EE", "value": 70, "name": "系列二" },
				{ "time": "FF", "value": 20, "name": "系列二" },
				{ "time": "GG", "value": 32, "name": "系列二" },
				{ "time": "HH", "value": 12, "name": "系列二" }
			],
		},
	},
	{
		name: '条形图',
		antVData: {
			type: {
				s: 'name',
				x: 'time', //x轴
				y: 'value',//y轴
				position: "time*value",//x*y
				transpose: true, //横向图表
			},
			data: [
				{ "time": "一月", "value": 20, "name": "系列二" },
				{ "time": "二月", "value": 20, "name": "系列二" },
				{ "time": "三月", "value": 120, "name": "系列二" },
				{ "time": "四月", "value": 30, "name": "系列二" },
				{ "time": "五月", "value": 70, "name": "系列二" },
				{ "time": "六月", "value": 20, "name": "系列二" },
				{ "time": "七月", "value": 32, "name": "系列二" },
				{ "time": "八月", "value": 12, "name": "系列二" },
				{ "time": "九月", "value": 92, "name": "系列二" },
				{ "time": "十月", "value": 44, "name": "系列二" }
			],
		},
	},
	{
		name: '横向对比图',
		antVData: {
			type: {
				s: 'name',
				x: 'time',
				y: 'value',
				position: "time*value",//x*y
				transpose: true, //横向图表
				diff: true, //左右对比
			},
			data: [
				{ "time": "分类一", "value": 500, "name": "系列一" },
				{ "time": "分类二", "value": 300, "name": "系列一" },
				{ "time": "分类三", "value": 100, "name": "系列一" },
				{ "time": "分类四", "value": 402, "name": "系列一" },
				{ "time": "分类五", "value": 425, "name": "系列一" },
				{ "time": "分类六", "value": 200, "name": "系列一" },
				{ "time": "分类一", "value": 500, "name": "系列二" },
				{ "time": "分类二", "value": 321, "name": "系列二" },
				{ "time": "分类三", "value": 386, "name": "系列二" },
				{ "time": "分类四", "value": 124, "name": "系列二" },
				{ "time": "分类五", "value": 325, "name": "系列二" },
				{ "time": "分类六", "value": 452, "name": "系列二" }
			],
		},
	},
	{
		name: '气球柱状图',
		antVData: {
			type: {
				s: 'name',
				x: 'time',
				y: 'value',
				position: "time*value",//x*y
				bubble: true
			},
			data: [
				{ "time": "一月", "value": 235, "name": "系列一" },
				{ "time": "二月", "value": 200, "name": "系列一" },
				{ "time": "三月", "value": 25, "name": "系列一" },
				{ "time": "四月", "value": 190, "name": "系列一" },
				{ "time": "五月", "value": 90, "name": "系列一" },
				{ "time": "六月", "value": 142, "name": "系列一" },
				{ "time": "七月", "value": 50, "name": "系列一" }
			],
		},
	},
	{
		name: '电池条形图',
		antVData: {
			type: {
				s: 'name',
				x: 'time', //x轴
				y: 'value',//y轴
				position: "time*value",//x*y
				transpose: true, //横向图表
			},
			data: [
				{ "time": "一月", "value": 20, "name": "系列二" },
				{ "time": "二月", "value": 20, "name": "系列二" },
				{ "time": "三月", "value": 120, "name": "系列二" },
				{ "time": "四月", "value": 30, "name": "系列二" },
				{ "time": "五月", "value": 70, "name": "系列二" },
				{ "time": "六月", "value": 20, "name": "系列二" },

			],
		},
	},
	{
		name: '立体柱状图',
		antVData: {
			type: {
				s: 'name',
				x: 'time',
				y: 'value',
				position: "time*value",//x*y
				xLine: true, //横向辅助线
				square: true //立体
			},
			data: [
				{ "time": "AA", "value": 1465, "name": "系列一" },
				{ "time": "BB", "value": 1956, "name": "系列一" },
				{ "time": "CC", "value": 2862, "name": "系列一" },
				{ "time": "DD", "value": 2015, "name": "系列一" },
				{ "time": "EE", "value": 1865, "name": "系列一" },
				{ "time": "FF", "value": 1426, "name": "系列一" },
				{ "time": "GG", "value": 1240, "name": "系列一" }
			],
		},
	},
]
// 折线图
const LINECHART = [
    {
        name: '基础折线图',
        antVData: {
            type: {
                s: 'name',
                x: 'time',
                y: 'value',
                position: "time*value",//x*y
                xLine: true,
                type: '1'
            },
            data: [
                { "time": "8:00", "value": 120, "name": "系列一" },
                { "time": "10:00", "value": 170, "name": "系列一" },
                { "time": "12:00", "value": 140, "name": "系列一" },
                { "time": "14:00", "value": 190, "name": "系列一" },
                { "time": "16:00", "value": 110, "name": "系列一" },
                { "time": "18:00", "value": 130, "name": "系列一" }
            ],
        },
    },
    {
        name: '平滑折线面积图',
        antVData: {
            type: {
                s: 'country',
                x: 'year',
                y: 'value',
                position: "year*value",//x*y
                xLine: true,
                areaLine: true,
            },
            data: [
                { country: 'Asia', year: '1750', value: 502 },
                { country: 'Asia', year: '1800', value: 635 },
                { country: 'Asia', year: '1850', value: 809 },
                { country: 'Asia', year: '1900', value: 5268 },
                { country: 'Asia', year: '1950', value: 4400 },
                { country: 'Asia', year: '1999', value: 3634 },
                { country: 'Asia', year: '2050', value: 947 },
                { country: 'Africa', year: '1750', value: 106 },
                { country: 'Africa', year: '1800', value: 107 },
                { country: 'Africa', year: '1850', value: 111 },
                { country: 'Africa', year: '1900', value: 1766 },
                { country: 'Africa', year: '1950', value: 221 },
                { country: 'Africa', year: '1999', value: 767 },
                { country: 'Africa', year: '2050', value: 133 },
                { country: 'Europe', year: '1750', value: 163 },
                { country: 'Europe', year: '1800', value: 203 },
                { country: 'Europe', year: '1850', value: 276 },
                { country: 'Europe', year: '1900', value: 628 },
                { country: 'Europe', year: '1950', value: 547 },
                { country: 'Europe', year: '1999', value: 729 },
                { country: 'Europe', year: '2050', value: 408 },
                { country: 'Oceania', year: '1750', value: 200 },
                { country: 'Oceania', year: '1800', value: 200 },
                { country: 'Oceania', year: '1850', value: 200 },
                { country: 'Oceania', year: '1900', value: 460 },
                { country: 'Oceania', year: '1950', value: 230 },
                { country: 'Oceania', year: '1999', value: 300 },
                { country: 'Oceania', year: '2050', value: 300 },
              ],
        },
    },
    {
        name: '平滑折线图',
        antVData: {
            type: {
                s: 'name',
                x: 'time',
                y: 'value',
                position: "time*value",//x*y
                xLine: true,
                type: '2'
            },
            data: [
                { "time": "8:00", "value": 120, "name": "系列一" },
                { "time": "10:00", "value": 340, "name": "系列一" },
                { "time": "12:00", "value": 220, "name": "系列一" },
                { "time": "14:00", "value": 410, "name": "系列一" },
                { "time": "16:00", "value": 170, "name": "系列一" },
                { "time": "18:00", "value": 110, "name": "系列一" },
                { "time": "8:00", "value": 105, "name": "系列二" },
                { "time": "10:00", "value": 140, "name": "系列二" },
                { "time": "12:00", "value": 295, "name": "系列二" },
                { "time": "14:00", "value": 180, "name": "系列二" },
                { "time": "16:00", "value": 240, "name": "系列二" },
                { "time": "18:00", "value": 115, "name": "系列二" }
            ],
        },
    },
    {
        name: '折柱混合图',
        antVData: {
            type: {
                s: 'call',
                x: 'time',
                y: 'waiting',
                position: "time*waiting",//x*y
                position2: "time*people",//x*y
                intervalLine: true
            },
            data: [
                { time: '10:10', call: 4, waiting: 2, people: 2 },
                { time: '10:15', call: 2, waiting: 6, people: 3 },
                { time: '10:20', call: 13, waiting: 2, people: 5 },
                { time: '10:25', call: 9, waiting: 9, people: 1 },
                { time: '10:30', call: 5, waiting: 2, people: 3 },
                { time: '10:35', call: 8, waiting: 2, people: 1 },
                { time: '10:40', call: 13, waiting: 1, people: 2 }
              ],
        },
    },
    {
        name: '数对折线图',
        antVData: {
            type: {
                s: 'country',
                x: 'year',
                y: 'value',
                position: "year*value",//x*y
                xLine: true,
                type: '3'
            },
            data: [
                { country: 'Asia', year: '1750', value: 502 },
                { country: 'Asia', year: '1800', value: 635 },
                { country: 'Asia', year: '1850', value: 809 },
                { country: 'Asia', year: '1900', value: 5268 },
                { country: 'Asia', year: '1950', value: 4400 },
                { country: 'Asia', year: '1999', value: 3634 },
                { country: 'Asia', year: '2050', value: 947 },
                { country: 'Africa', year: '1750', value: 106 },
                { country: 'Africa', year: '1800', value: 107 },
                { country: 'Africa', year: '1850', value: 111 },
                { country: 'Africa', year: '1900', value: 1766 },
                { country: 'Africa', year: '1950', value: 221 },
                { country: 'Africa', year: '1999', value: 767 },
                { country: 'Africa', year: '2050', value: 133 },
                { country: 'Europe', year: '1750', value: 163 },
                { country: 'Europe', year: '1800', value: 203 },
                { country: 'Europe', year: '1850', value: 276 },
                { country: 'Europe', year: '1900', value: 628 },
                { country: 'Europe', year: '1950', value: 547 },
                { country: 'Europe', year: '1999', value: 729 },
                { country: 'Europe', year: '2050', value: 408 },
                { country: 'Oceania', year: '1750', value: 200 },
                { country: 'Oceania', year: '1800', value: 200 },
                { country: 'Oceania', year: '1850', value: 200 },
                { country: 'Oceania', year: '1900', value: 460 },
                { country: 'Oceania', year: '1950', value: 230 },
                { country: 'Oceania', year: '1999', value: 300 },
                { country: 'Oceania', year: '2050', value: 300 },
            ],
        },
    },
]
// 散点图
const SCATTERDIAGRAM = [
	{
		name: '基础散点图',
		antVData: {
			type: {
				s: 'category',
				x: 'name',
				y: 'value',
				xLine: true,
				position: 'name*value'
			},
			data: [
				{ "name": "星期一", "value": 26758, "category": "系列一" },
				{ "name": "星期二", "value": 15001, "category": "系列一" },
				{ "name": "星期三", "value": 28932, "category": "系列一" },
				{ "name": "星期四", "value": 16758, "category": "系列一" },
				{ "name": "星期五", "value": 25001, "category": "系列一" },
				{ "name": "星期六", "value": 18932, "category": "系列一" },
				{ "name": "星期日", "value": 11389, "category": "系列一" },
				{ "name": "星期一", "value": 3758, "category": "系列二" },
				{ "name": "星期二", "value": 13001, "category": "系列二" },
				{ "name": "星期三", "value": 20932, "category": "系列二" },
				{ "name": "星期四", "value": 36245, "category": "系列二" },
				{ "name": "星期五", "value": 31563, "category": "系列二" },
				{ "name": "星期六", "value": 36389, "category": "系列二" },
				{ "name": "星期日", "value": 41389, "category": "系列二" }
			],
		},
	},
	{
		name: '折线散点图',
		antVData: {
			type: {
				s: 'category',
				x: 'name',
				y: 'value',
				xLine: true,
				doubleLine: true,
				position: 'name*value'
			},
			data: [
				{ "name": "2.1", "value": 16758, "category": "系列一" },
				{ "name": "2.2", "value": 15001, "category": "系列一" },
				{ "name": "2.3", "value": 28932, "category": "系列一" },
				{ "name": "2.4", "value": 6758, "category": "系列一" },
				{ "name": "2.5", "value": 25001, "category": "系列一" },
				{ "name": "2.6", "value": 18932, "category": "系列一" },
				{ "name": "2.1", "value": 6758, "category": "line" },
				{ "name": "2.2", "value": 11001, "category": "line" },
				{ "name": "2.3", "value": 20932, "category": "line" },
				{ "name": "2.4", "value": 36245, "category": "line" },
				{ "name": "2.5", "value": 31563, "category": "line" },
				{ "name": "2.6", "value": 36389, "category": "line" }
			],
		},
	},
]
// 饼图
const PIECHART = [
	{
		name: '南丁格尔玫瑰图',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 1
			},
			data: [
				{ "value": 13, "name": "事故灾难" },
				{ "value": 15, "name": "社会安全" },
				{ "value": 40, "name": "其他" },
				{ "value": 25, "name": "公共卫生" },
				{ "value": 7, "name": "自然灾害" }
			],
		},
	},
	{
		name: '圆环进度',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 2
			},
			data: [
				{ "value": 70, "name": "完成" },
			],
		},
	},
	{
		name: '基础指标',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 3
			},
			data: [
				{
					"value": 7,
					"name": "橙色预警"
				},
				{
					"value": 18,
					"name": "红色预警"
				},
				{
					"value": 28,
					"name": "黄色预警"
				},
				{
					"value": 21,
					"name": "蓝色预警"
				}
			]
		},
	},
	{
		name: '年龄分布',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 4
			},
			data: [
				{ "value": 7, "name": "18岁以下" },
				{ "value": 18, "name": "18-25岁" },
				{ "value": 21, "name": "25-40岁" }
			],
		},
	},
	{
		name: '扇形饼',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 5
			},
			data: [
				{ "value": 600.58, "name": "分类一" },
				{ "value": 1100.58, "name": "分类二" },
				{ "value": 1200.58, "name": "分类三" },
				{ "value": 1300.58, "name": "分类四" },
				{ "value": 1400.58, "name": "分类五" }
			],
		},
	},
	{
		name: '预警等级分布',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				classification: 6
			},
			data: [
				{ "value": 7, "name": "橙色预警" },
				{ "value": 18, "name": "红色预警" },
				{ "value": 28, "name": "黄色预警" },
				{ "value": 21, "name": "蓝色预警" }
			],
		},
	},
]
// 雷达图
const RADARCHART = [
	{
		name: '分割雷达',
		antVData: {
			type: {
				// s: '',
				// x: '',
				// y: '',
				classification: 1
			},
			data: [
				{ item: 'Design', a: 70, b: 30 },
				{ item: 'Development', a: 60, b: 70 },
				{ item: 'Marketing', a: 50, b: 60 },
				{ item: 'Users', a: 40, b: 50 },
				{ item: 'Test', a: 60, b: 70 },
				{ item: 'Language', a: 70, b: 50 },
				{ item: 'Technology', a: 50, b: 40 },
				{ item: 'Support', a: 30, b: 40 },
				{ item: 'Sales', a: 60, b: 40 },
				{ item: 'UX', a: 50, b: 60 },
			],
			//  [
			// 	{
			// 		"indicator": [
			// 			{ "name": "维度一", "max": 3000 },
			// 			{ "name": "维度二", "max": 3000 },
			// 			{ "name": "维度三", "max": 3000 },
			// 			{ "name": "维度四", "max": 3000 },
			// 			{ "name": "维度五", "max": 3000 },
			// 			{ "name": "维度六", "max": 3000 }
			// 		],
			// 		"data": [
			// 			{ "维度一": 2200, "维度二": 1400, "维度三": 800, "维度四": 1800, "维度五": 1400, "维度六": 1400, "name": "系列一" },
			// 			{ "维度一": 1400, "维度二": 2200, "维度三": 1600, "维度四": 800, "维度五": 1000, "维度六": 1900, "name": "系列二" }
			// 		]
			// 	}
			// ],
		},
	},
	{
		name: '基本雷达',
		antVData: {
			type: {
				// s: 'name',
				// x: '',
				// y: '',
				classification: 2
			},
			data: [
				{ item: '维度一', a: 70, b: 30 },
				{ item: '维度二', a: 60, b: 70 },
				{ item: 'Marketing', a: 50, b: 60 },
				{ item: 'Users', a: 40, b: 50 },
				{ item: 'Test', a: 60, b: 70 },
				{ item: 'Language', a: 70, b: 50 },
				{ item: 'Technology', a: 50, b: 40 },
				{ item: 'Support', a: 30, b: 40 },
				{ item: 'Sales', a: 60, b: 40 },
				{ item: 'UX', a: 50, b: 60 },
			],
		},
	},
	{
		name: '多边形雷达',
		antVData: {
			type: {
				// s: 'name',
				// x: '',
				// y: '',
				classification: 3
			},
			data: [
				{ item: '维度一', a: 70, b: 30 },
				{ item: '维度二', a: 60, b: 70 },
				{ item: 'Marketing', a: 50, b: 60 },
				{ item: 'Users', a: 40, b: 50 },
				{ item: 'Test', a: 60, b: 70 },
				{ item: 'Language', a: 70, b: 50 },
				{ item: 'Technology', a: 50, b: 40 },
				{ item: 'Support', a: 30, b: 40 },
				{ item: 'Sales', a: 60, b: 40 },
				{ item: 'UX', a: 50, b: 60 },
			],
		},
	}
]
// 其他
const OTHERFIGURES = [
	{
		name: '基础仪表',
		antVData: {
			type: {
				s: '',
				name: 'name',
				value: 'value'
			},
			data: [
				{ "value": 44, "name": "系列一" }
			],
		},
	},
	// {
	//     name: '基础关系',
	// },
	// {
	//     name: '基础水球',
	// },
	{
		name: '基础漏斗',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value'
			},
			data: [
				{ "value": 60, "name": "访问" },
				{ "value": 40, "name": "咨询" },
				{ "value": 20, "name": "订单" },
				{ "value": 80, "name": "点击" },
				{ "value": 100, "name": "展现" }
			],
		},
	},
	{
		name: '渐变仪表',
		antVData: {
			type: {
				s: '',
				name: 'name',
				value: 'value'
			},
			data: [
				{ "value": 50, "name": "系列一" }
			],
		},
	},
	{
		name: '词',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value'
			},
			data: [
				{ "name": "Macys", "value": 6181 },
				{ "name": "Amy", "value": 4386 },
				{ "name": "Jurassic World", "value": 4055 },
				{ "name": "Charter Communications", "value": 2467 },
				{ "name": "Chick Fil A", "value": 2244 },
				{ "name": "Planet Fitness", "value": 1898 },
				{ "name": "Macys", "value": 6181 },
				{ "name": "Amy Schumer", "value": 4386 },
				{ "name": "Jurassic World", "value": 4055 },
				{ "name": "Charter Communications", "value": 2467 },
				{ "name": "Chick Fil A", "value": 2244 },
				{ "name": "Planet Fitness", "value": 1898 }
			],
		},
	}
]

// 2d地图
const twmapList = [
	{
		name: '中国地图省级',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '2DMAP1'
			},
			data: [],
		},
	},
	{
		name: '中国地图市级',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '2DMAP2'
			},
			data: [],
		},
	},
	{
		name: '地图交互',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '2DMAP3'
			},
			data: [],
		},
	},
	{
		name: '连续填充图',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '2DMAP4'
			},
			data: [],
		},
	},
]
// 3d地图
const thmapList = [
	{
		name: '3D建筑面',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '3DMAP1'
			},
			data: [],
		},
	},
	{
		name: '3D几何样式',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '3DMAP2'
			},
			data: [],
		},
	},
	{
		name: '漂浮地图板块',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '3DMAP3'
			},
			data: [],
		},
	},
	{
		name: '点图层',
		antVData: {
			type: {
				s: '',
				x: 'name',
				y: 'value',
				type: '3DMAP4'
			},
			data: [],
		},
	},

]
const HistogramArrary: any = [];
const LineChartArrary: any = [];
const ScatterDiagramArrary: any = [];
const PieChartArrary: any = [];
const RadarChartArrary: any = [];
const OtherFiguresArrary: any = [];
const twmapArrary: any = [];
const thmapArrary: any = [];

//3D地图
(function all3dmapIcon() {
	const files = require.context('../assets/svgIcons2/3dmap/', false, /.*$/);
	// console.log(files); // ./test.png
	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: thmapList[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/3dmap/" + path.slice(2)),
			type: '3dmap',
			antVData:thmapList[index].antVData
		}
		thmapArrary.push(data)
	})
})();
//2D地图
(function allmapIcon() {
	const files = require.context('../assets/svgIcons2/2dmap/', false, /.png$/);
	// console.log(files); // ./test.png
	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: twmapList[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/2dmap/" + path.slice(2)),
			type: '2dmap',
			antVData:twmapList[index].antVData
		}
		twmapArrary.push(data)
	})
})();

// 柱状图
(function allHistogramIcon() {
	const files = require.context('../assets/svgIcons2/Histogram/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: HISTOGRAM[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/Histogram/" + path.slice(2)),
			type: 'bar',
			antVData: HISTOGRAM[index].antVData
		}
		HistogramArrary.push(data)
	})
})();

// 折线图
(function allLineChartIcon() {
	const files = require.context('../assets/svgIcons2/LineChart/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: LINECHART[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/LineChart/" + path.slice(2)),
			type: 'line',
			antVData: LINECHART[index].antVData
		}
		LineChartArrary.push(data)
	})
})();

// 散点图
(function allScatterDiagramIcon() {
	const files = require.context('../assets/svgIcons2/ScatterDiagram/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: SCATTERDIAGRAM[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/ScatterDiagram/" + path.slice(2)),
			type: 'scatter',
			antVData: SCATTERDIAGRAM[index].antVData
		}
		ScatterDiagramArrary.push(data)
	})
})();

// 饼图
(function allPieChartIcon() {
	const files = require.context('../assets/svgIcons2/PieChart/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: PIECHART[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/PieChart/" + path.slice(2)),
			type: 'pie',
			antVData: PIECHART[index].antVData
		}
		PieChartArrary.push(data)
	})
})();

// 雷达图
(function allRadarChartIcon() {
	const files = require.context('../assets/svgIcons2/RadarChart/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: RADARCHART[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/RadarChart/" + path.slice(2)),
			type: 'fold',
			antVData: RADARCHART[index].antVData
		}
		RadarChartArrary.push(data)
	})
})();

// 其他
(function allOtherFiguresIcon() {
	const files = require.context('../assets/svgIcons2/OtherFigures/', false, /.png$/);
	// console.log(files); // ./test.png

	files.keys().forEach((path, index) => {
		// console.log(path, index); // ./test.png
		let data = {
			key: index,
			shape: true,
			title: OTHERFIGURES[index].name,
			source: "/freeeditor/img/" + path.slice(2),
			url: require("../assets/svgIcons2/OtherFigures/" + path.slice(2)),
			type: 'bar',
			antVData: OTHERFIGURES[index].antVData
		}
		OtherFiguresArrary.push(data)
	})
})();

export {twmapArrary, thmapArrary, HistogramArrary, LineChartArrary, ScatterDiagramArrary, PieChartArrary, OtherFiguresArrary, RadarChartArrary };

// export const BAR_CHART_LIST = [
//     (function allShapesIcon() {
//         const files = require.context('../assets/svgIcons2/Histogram/', false, /.png$/);
//         console.log(files); // ./test.png

//         // files.keys().forEach((path, index) => {
//         //     // console.log(path, index); // ./test.png
//         //     let data = {
//         //         key: index,
//         //         shape: true,
//         //         source: require("../assets/svgIcons2/柱状图/" + path.slice(2)),
//         //     }
//         //     BAR_CHART_LIST.push(data)
//         // })
//     })(),
// ]

// export const LINE_CHART_LIST = [
//     {
//         name: '点线平滑折线图',
//         icon: 'icon-jibenzhuzi',
//         width: 400,
//         height: 77,
//         type: 'bar',
//         content: '<p style=\'\'><strong><span style=\'font-size: 48px\'>在此处添加标题</span></strong></p>',
//     },
//     {
//         name: '基础折线图',
//         icon: 'icon-jibenzhexian',
//         width: 240,
//         height: 56,
//         type: 'line',
//         content: '<p style=\'\'><span style=\'font-size: 24px\'>在此处添加副标题</span></p>',
//     },
//     {
//         name: '心电图',
//         icon: 'icon-bingzhuangtu',
//         width: 420,
//         height: 340,
//         type: 'pie',
//         content: "<p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p><p style='text-align: center;'><span style='font-size: 24px'>在此处输入内容</span></p>",
//     }

// ]
