export const ELEMENT_TYPE_ZH = {
  text: '文本',
  image: '图片',
  shape: '形状',
  line: '线条',
  chart: '图表',
  table: '表格',
  video: '视频',
  audio: '音频',
  custom: '自定义组件',
}

export const MIN_SIZE = {
  text: 20,
  image: 20,
  shape: 1,
  chart: 100,
  table: 20,
  video: 250,
  audio: 20,
  custom: 100,
}