
export const getRequest = (url: string, params: any) => {
    let token = localStorage.getItem("token");
    if (typeof params === 'string') {
        url = url + `/${params}`
    } else {
        let arr = [];
        for (let key in params) {
            arr.push(key + '=' + params[key])
        }
        url = url + arr.join('&')
    }

    return fetch(url, {
        headers: { 'Authorization': token ? token : '' }
    })
        .then(function (response) {
            // console.log(6666666, response)
            return response.json();
        })
        .catch(err => console.log(err))

}