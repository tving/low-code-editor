export default {
    officeType: 7, //类型 主题色 
    // 还原单个文件
    // 全部还原
    // 批量还原
    // 单个删除
    // 批量删除
    // 全部删除
    // 清空回收站

    // 首页
    homesharelinumber: 4, // 每行有多少个
    homeshareproportion: 169 / 235, // 图片比例
    homesharelimargin: 17, // 每个li的边距
    hometitlejustifyContent: 'center', // 文字显示方位 center居中
    homeshareTopiconsize: 18, // 图标字体大小
    homelistswitch: true, // 是否显示列表模式
    homehomebottomjiahao: false, // 最下方是否出现加号
    homehomenoneimg: '', // 首页没有数据时的图片
    homenewjiahao: '', // 图表第一个新增小方框
    homebianjiiconfz: '', // 右上角进入编辑图标字体大小(包括列表模式)
    homeshareTopshousuo: '#4355dc', // 搜索点击后背景颜色
    homeshareTopiconcolor: '', // 图标列表切换字体颜色
    homehomeqingkong: '', // 是否显示清空按钮
    homehomenewcard: true, // 图表模式第一个新增是否显示
    homesharetitlefontsize: '', // 名字字体大小
    homesharetitlecolor: '', // 名字字体颜色
    homesharetitleiconfz: '', // 修改名字图标大小
    // 分享
    shareshareproportion: 169 / 235, // 图片比例 (必传)
    sharesharelimargin: 17, // 每一个li外边距 (必传)
    sharesharelinumber: 4, // 每一行有几个 (必传)
    sharetitlejustifyContent: 'center', // 标题居中方式
    sharelistswitch: true, // 列表切换
    shareshareTopThumb: '#4355dc', // 滑块颜色
    shareshareTopshousuo: '#4355dc', // 搜索点击颜色
    shareshareToptitlecolor: '', // 头部多少数据颜色
    shareshareToptitlesize: '', // 头部多少数据字体大小
    shareshareTopiconcolor: '', // 图标列表切换字体颜色
    shareshareTopiconsize: '', // 切换字体大小
    sharesharetitlefontsize: '', // 名字字体大小
    sharesharetitlecolor: '#fff', // 名字字体颜色
    sharesharetitleiconfz: '', // 修改名字图标大小
    shareshareppt: '', // 人物头像在下面true自由编辑器
    // 回收站
    delsharelinumber: 4, // 每行有多少个
    delshareproportion: 169 / 235, // 图片比例
    delsharelimargin: 17, // 每个li的边距
    deltitlejustifyContent: 'center', // 文字显示方位 center居中
    dellistswitch: true, // 列表切换
    delshareTopshousuo: '#4355dc', // 搜索点击颜色
    delshareTopiconcolor: '', // 图标列表切换字体颜色
    delshareTopiconsize: '', // 切换字体大小
    delsharetitlefontsize: '', // 名字字体大小
    delsharetitlecolor: '#fff', // 名字字体颜色
}