import mitt, { Emitter } from 'mitt'
import { CreateElementSelectionData, CreatingLineElement, CreatingShapeElement } from '@/types/edit'

export const enum EmitterEvents {
  RICH_TEXT_COMMAND = 'RICH_TEXT_COMMAND',
  OPEN_CHART_DATA_EDITOR = 'OPEN_CHART_DATA_EDITOR',
  OPEN_LATEX_EDITOR = 'OPEN_LATEX_EDITOR',
  SAVE_PROJECT = 'SAVE_PROJECT',
  INSERT_ELEMENT = 'INSERT_ELEMENT',
  CREATE_CUSTOM = 'CREATE_CUSTOM',
  GET_CUSTOM_LIST = 'GET_CUSTOM_LIST',
  SET_CANVAS_SIZE = 'SET_CANVAS_SIZE',
}

export interface RichTextCommand {
  command: string;
  value?: string;
}
export interface GetList {
  type: number;
}
type Events = {
  [EmitterEvents.RICH_TEXT_COMMAND]: RichTextCommand | RichTextCommand[];
  [EmitterEvents.OPEN_CHART_DATA_EDITOR]: void;
  [EmitterEvents.OPEN_LATEX_EDITOR]: void;
  [EmitterEvents.SAVE_PROJECT]: void;
  [EmitterEvents.CREATE_CUSTOM]: void;
  [EmitterEvents.GET_CUSTOM_LIST]: GetList;
  [EmitterEvents.INSERT_ELEMENT]: CreateElementSelectionData;
  [EmitterEvents.SET_CANVAS_SIZE]: void;

} 

const emitter: Emitter<Events> = mitt<Events>()

export default emitter