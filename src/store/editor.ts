import { defineStore } from 'pinia'

export interface EditorState {
    cloudToggle: boolean;
    sourceUrl: string;
    audioUrl: string;
    sourceType: string;
    sourcePreview: boolean;
    mediaCategroy: string;
}

export const useEditorStore = defineStore('editor', {
    state: (): EditorState => ({
        cloudToggle: false, // 是否打开媒体库
        sourceUrl: "",
        sourceType: "",
        audioUrl: "",
        sourcePreview: false,//是否是预览
        mediaCategroy: ""
    }),

    actions: {
        setCloudToggle(cloudToggle: boolean) {
            this.cloudToggle = cloudToggle
        },
        setSourceUrl(sourceUrl: string) {
            this.sourceUrl = sourceUrl
        },
        setAudioUrl(audioUrl: string) {
            this.audioUrl = audioUrl
        },
        setSourceType(sourceType: string) {
            this.sourceType = sourceType
        },
        setSourcePreview(sourcePreview: boolean) {
            this.sourcePreview = sourcePreview
        },
        setMediaCategroy(mediaCategroy: string) {
            this.mediaCategroy = mediaCategroy
        },


    },
})