import { defineStore } from "pinia";
import { CreatingElement } from "@/types/edit";
import { ToolbarState } from "@/types/toolbar";
import { SYS_FONTS } from "@/configs/font";
import { ENV } from "@/configs/env";
import { TextAttrs, defaultRichTextAttrs } from "@/utils/prosemirror/utils";
import { isSupportFont } from "@/utils/font";

import { useSlidesStore } from "./slides";

export interface MainState {
  activeElementIdList: string[];
  handleElementId: string;
  handleGroupId: string;
  activeGroupElementId: string;
  canvasPercentage: number;
  canvasScale: number;
  thumbnailsFocus: boolean;
  editorAreaFocus: boolean;
  disableHotkeys: boolean;
  showGridLines: boolean;
  creatingElement: CreatingElement | null;
  availableFonts: typeof SYS_FONTS;
  globalEnv: string;
  toolbarState: ToolbarState;
  clipingImageElementId: string;
  isScaling: boolean;
  richTextAttrs: TextAttrs;
  selectedTableCells: string[];
  editingShapeElementId: string;
  selectedSlidesIndex: number[];
  currentTab: number;
  customList: any;
  canvasWidth: number;
  canvasHeight: number;
  dragIndex: any;
  initCanvasScale: boolean;
  del: boolean;
  checkAlls: boolean;
  delone: boolean;
  deloneitem: any;
  delmore: boolean;
  delmoreitem: any;
  deltype: number;
  distributemore: boolean;
  currentElementComponent: string;
  indexImage: string;
  disTypeteamwork: number;
  currentLogUserId: number | string;
  currentLogData: any;
  folderID: number | string;
  openkeysid: string[];
  pptTitle: string;
  switchTime: number;
  MultiPage: boolean, 
  EditorType: string | number,
  // 协同代码
  handleElementUser: string;
  handleElementOtherIdList: any;
  showRuler: boolean; // 标尺开关
  canvasDragged: boolean;
}

export const useMainStore = defineStore("main", {
  state: (): MainState => ({
    activeElementIdList: [], // 被选中的元素ID集合，包含 handleElementId
    handleElementId: "", // 正在操作的元素ID
    handleGroupId: "", //正在操作的组合元素ID
    activeGroupElementId: "", // 组合元素成员中，被选中可独立操作的元素ID
    canvasPercentage: 90, // 画布可视区域百分比
    canvasScale: 1, // 画布缩放比例（基于宽度1000px）
    initCanvasScale: false,
    thumbnailsFocus: false, // 左侧导航缩略图区域聚焦
    editorAreaFocus: false, //  编辑区域聚焦
    disableHotkeys: false, // 禁用快捷键
    showGridLines: false, // 显示网格线
    creatingElement: null, // 正在插入的元素信息，需要通过绘制插入的元素（文字、形状、线条、图表、图片、自定义组件）
    availableFonts: [], // 当前环境可用字体
    globalEnv: ENV, //当前接口环境
    toolbarState: "slideDesign", // 右侧工具栏状态
    clipingImageElementId: "", // 当前正在裁剪的图片ID
    richTextAttrs: defaultRichTextAttrs, // 富文本状态
    selectedTableCells: [], // 选中的表格单元格
    isScaling: false, // 正在进行元素缩放
    editingShapeElementId: "", // 当前正处在编辑文字状态的形状ID
    selectedSlidesIndex: [], // 当前被选中的页面索引集合
    currentTab: 1, // 当前是全部页面还是项目设置
    customList: [], //自定义组件列表
    canvasWidth: 1000,
    canvasHeight: 1000 * useSlidesStore().viewportRatio,
    dragIndex: null, // 拖拽到当前页
    checkAlls: false, // 是否全选
    del: false, // 判断是否弹出删除框
    deltype: 0, // 删除模式
    delone: false, // 删除一个
    deloneitem: "", // 删除一个数据
    delmore: false, // 删除多个
    delmoreitem: [], // 删除多个数据
    indexImage: "", // 保存时的缩略图
    distributemore: false, // 添加更多分享者
    currentElementComponent: "Contentlist", // 判断显示的组件
    disTypeteamwork: 0, // 已接收已派发列表
    currentLogUserId: 0, //当前登录用户的id
    currentLogData: null, // 用户所有信息(对象)
    folderID: -1, // 文件夹id
    openkeysid: [""], // 判断展开的列表
    pptTitle: "",
    switchTime: 2500,
    MultiPage: false, //是否为多页面
    EditorType: '1',   //编辑器类型 （可视化 1，自由编辑 2，ppt 3）
    // 协同代码
    handleElementUser: "", //当前元素操作人
    handleElementOtherIdList: [
      {
        userid: "",
        username: "",
        handleElementId: "",
      },
    ], //当前操作的元素idlist
    showRuler: false,     // 显示标尺
    canvasDragged: false, // 画布被拖拽移动
  }),

  getters: {
    activeElementList(state) {
      const slidesStore = useSlidesStore();
      const currentSlide = slidesStore.currentSlide;
      if (!currentSlide || !currentSlide.elements) return [];
      return currentSlide.elements.filter((element) =>
        state.activeElementIdList.includes(element.id)
      );
    },

    handleElement(state) {
      const slidesStore = useSlidesStore();
      const currentSlide = slidesStore.currentSlide;
      if (!currentSlide || !currentSlide.elements) return null;
      return (
        currentSlide.elements.find(
          (element) => state.handleElementId === element.id
        ) || null
      );
    },
  },

  actions: {
    setSwitchTime(name: number) {
      this.switchTime = name;
    },
    setTitle(name: string) {
      this.pptTitle = name;
    },
    setIndexImage(name: string) {
      this.indexImage = name;
    },
    setDragIndex(index: any) {
      this.dragIndex = index;
    },
    setCustomList(list: any) {
      this.customList = list;
    },
    setCurrentTab(num: number) {
      this.currentTab = num;
    },
    setActiveElementIdList(activeElementIdList: string[]) {
      if (activeElementIdList.length === 1)
        this.handleElementId = activeElementIdList[0];
      else this.handleElementId = "";

      this.activeElementIdList = activeElementIdList;
    },

    setHandleElementId(handleElementId: string) {
      this.handleElementId = handleElementId;
    },
    setHandleGroupId(id: string) {
      this.handleGroupId = id;
    },

    setActiveGroupElementId(activeGroupElementId: string) {
      this.activeGroupElementId = activeGroupElementId;
    },

    setCanvasPercentage(percentage: number) {
      this.canvasPercentage = percentage;
    },

    setCanvasScale(scale: number) {
      this.canvasScale = scale;
    },
    setInitCanvasScale(scale: boolean) {
      this.initCanvasScale = scale;
    },

    setThumbnailsFocus(isFocus: boolean) {
      this.thumbnailsFocus = isFocus;
    },

    setEditorareaFocus(isFocus: boolean) {
      this.editorAreaFocus = isFocus;
    },

    setDisableHotkeysState(disable: boolean) {
      this.disableHotkeys = disable;
    },

    setGridLinesState(show: boolean) {
      this.showGridLines = show;
    },

    setCreatingElement(element: CreatingElement | null) {
      this.creatingElement = element;
    },

    setAvailableFonts() {
      this.availableFonts = SYS_FONTS.filter((font) =>
        isSupportFont(font.value)
      );
    },

    setToolbarState(toolbarState: ToolbarState) {
      this.toolbarState = toolbarState;
    },

    setClipingImageElementId(elId: string) {
      this.clipingImageElementId = elId;
    },

    setRichtextAttrs(attrs: TextAttrs) {
      this.richTextAttrs = attrs;
    },

    setSelectedTableCells(cells: string[]) {
      this.selectedTableCells = cells;
    },

    setScalingState(isScaling: boolean) {
      this.isScaling = isScaling;
    },

    setEditingShapeElementId(ellId: string) {
      this.editingShapeElementId = ellId;
    },

    updateSelectedSlidesIndex(selectedSlidesIndex: number[]) {
      this.selectedSlidesIndex = selectedSlidesIndex;
    },
    storeCurrentLogUserId(userId: number | string) {
      this.currentLogUserId = userId;
    },
    storeCurrentLogData(data: any) {
      this.currentLogData = data;
    },
    storeFolderID(id: number | string) {
      this.folderID = id;
    },
    storeopenkeysid(id: string[]) {
      this.openkeysid = id;
    },
    setCanvasDragged(isDragged: boolean) {
      this.canvasDragged = isDragged
    },
    // 协同代码
    setHandleElementUser(name: string) {
      this.handleElementUser = name;
    },
    setHandleElementOtherIdList(idlist: any) {
      this.handleElementOtherIdList = idlist;
    },
    // 显示标尺开关
    setRulerState(show: boolean) {
      this.showRuler = show;
    },
  },
});
