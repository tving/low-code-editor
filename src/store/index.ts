import { useMainStore } from './main'
import { useSlidesStore } from './slides'
import { useSnapshotStore } from './snapshot'
import { useKeyboardStore } from './keyboard'
import { useScreenStore } from './screen'
import { useEditorStore } from './editor'

export {
  useMainStore,
  useSlidesStore,
  useSnapshotStore,
  useKeyboardStore,
  useScreenStore,
  useEditorStore
}