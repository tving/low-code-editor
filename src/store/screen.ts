import { defineStore } from 'pinia'

export interface ScreenState {
  screening: boolean;
  renderdata: boolean;
  editCanvasLock: boolean;
  autoplay: boolean;
}

export const useScreenStore = defineStore('screen', {
  state: (): ScreenState => ({
    screening: false, // 是否进入放映状态
    renderdata: true, // 退出放映后是否请求数据
    editCanvasLock: true, //进入放映不监听画布尺寸变化
    autoplay: false, //自动播放开关
  }),

  actions: {
    setAutoplay(screening: boolean) {
      this.autoplay = screening
    },
    setScreening(screening: boolean) {
      this.screening = screening
    },
    setRenderData(renderdata: boolean) {
      this.renderdata = renderdata
    },
    setEditCanvasLock(data: boolean) {
      this.editCanvasLock = data
    },
  },
})