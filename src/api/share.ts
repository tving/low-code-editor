//分享所有请求
import API from "@/api/API";
import axios from "@/utils/axios";

export const doStoreShareMember = async (options:any) => {
  return await axios.post(API.STORE_MEMBER_LIST,options);
};

export const doCancleShare = async (options:any) => {
  return await axios.get(API.CANCLE_SHARE + options.officeId);
};