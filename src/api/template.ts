//模板相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getTemplateList = async () => {
  return await axios.get(API.TEMPLATE_LIST);
};