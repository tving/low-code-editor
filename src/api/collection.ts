//我的收藏相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getCollectionList = async () => {
  return await axios.get(API.COLLECTION_LIST);
};