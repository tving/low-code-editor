/**
 * 接口地址管理
 */

export default {
  TEMPLATE_LIST: "",  //模板列表
  DESIGN_LIST: "",  //我的设计列表
  COLLECTION_LIST: "",  //我的收藏列表
  TEAMWORK_LIST: "",  //协同办公列表
  NOTICE_LIST: "",  //消息列表


  ADD_CONTENT: '/space/office/spaceOffice/saveSpaceOffice',   //添加内容
  CONTENT_LIST: '/space/office/spaceOffice/pageList',   //首页内容列表
  DELET_CONTENT: '/space/office/spaceOffice/delSpaceOffice/',   //删除内容  单个删除
  DELET_CONTENT_LIST: '/space/office/spaceOffice/delSpaceOfficeList',   //删除内容  批量删除

  EDIT_FILE: '/space/office/spaceOffice/editSpaceOffice', //编辑内容 

  PASTE_FILE: '/space/office/spaceOffice/copyPaste',   //删除内容  批量删除


  ALL_USER_LIST: '/api/sc/user/list',   //所有用户列表
  PAGE_USER_LIST: '/api/sc/user/page',   //用户列表分页
  // CURRENT_USER_INFO: 'space/user/hhxsUser/getUserInfo',   ////获取当前登录的用户信息
  CURRENT_USER_INFO: '/hhxs-ssc/sc/user/get/user/info',   ////获取当前登录的用户信息
  MY_IMG_LIST: '/hhxs-ssc/sc/fileInfo/new/list',   //图片资源列表
  VIDEO_IMG_LIST: '/hhxs-ssc/sc/newFileInfo/getFileList',   //图片和视频 有类型区分资源列表


  GET_CUNSTOM_USERINFO:'api/sc/user/get/user/info',         /// 获取用户信息
  ADD_CUNSTOM_TEMPLATE:'hhxs-app/app/ssc/comp/save',         /// 添加自定义组件 
  DELETE_TEMPLATE:'hhxs-app/app/ssc/comp/del',         /// 删除自定义组件 
  CHANGE_CUNSTOM_TEMPLATE:'hhxs-app/app/ssc/comp/sharePrivateMutualRotation', /// 公开或者改为私有自定义组件 
  EDIT_CUNSTOM_TEMPLATE:'hhxs-app/app/ssc/comp/update',         /// 修改自定义组件 
  GET_CUNSTOM_TEMPLATE:'hhxs-app/app/ssc/comp/list',         /// 获取自定义组件列表 
  

  STORE_MEMBER_LIST: '/space/office/spaceOffice/editSharer',   //保存分享人员列表i
  CANCLE_SHARE: '/space/office/spaceOffice/quitShare/',   //取消共享
  

  RECYCLE_LIST: "/space/office/spaceOfficeRecycle/pageList",  //回收站列表
  FILE_REDUTION: "/space/office/spaceOfficeRecycle/recovery",  // 回收站文件恢复
  AFOND_DELETE_FILE: "/space/office/spaceOfficeRecycle/completelyDelete",  //回收站彻底删除

  GET_ONE: "/space/office/spaceOffice/querySpaceOffice/", // 获取一条
  GET_ONE_PREVIEW: "/space/office/spaceOffice/preview/", // 获取一条预览用

  // RECYCLE_LIST: "/space/office/spaceOfficeRecycle/pageList",  //回收站列表
  SHARE_LIST: "/space/office/spaceOffice/pageList", // 数据列表(分享)
  HOME_NEW: "",
  EDIT_MODIFY: "/space/office/spaceOffice/editSpaceOffice", // 修改接口
  // RECYCLE_LIST:"/sc/newFileInfo/latestUploadList" ,  //回收站列表

  ADD_EDITOR: '/hhxs-ssc/sc/cloudDiskFileInfo/addUplodFile',  //新增
  EDIT_EDITOR: '/hhxs-ssc/sc/fileInfo/edit',                  //编辑
  GET_EDITOR: '/hhxs-ssc/sc/newFileInfo/getOfficeFileContent', //获取
  ADD_UPLOAD_FILE: '/hhxs-ssc/sc/cloudDiskFileInfo/addUploadFileBatch', //批量新增文件
  obsMpcController: '/space/obs/obs/obsMpcController/create',      // 截图
  delete_editor: '/hhxs-ssc/sc/cloudDiskFileInfo/delete', //删除
  deleteFiles: '/hhxs-ssc/sc/newFileInfo/deleteFiles',    //批量删除
};
