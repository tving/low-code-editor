//z=自定义模板相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";
// 获取自定义模板列表
export const getCustomTempaleList = async (options: any) => {
  return await axios.post(API.GET_CUNSTOM_TEMPLATE,options);
};

// 删除自定义模板
export const deleteCustomTempaleList = async (options: any) => {
  return await axios.get(API.DELETE_TEMPLATE +'/'+ options.id);
};

// 修改自定义模板的公有属性
export const changeCustomTempaleList = async (options: any) => {
  return await axios.post(API.CHANGE_CUNSTOM_TEMPLATE,options);
};

// 编辑自定义模板
export const editCustomTempaleList = async (options: any) => {
  return await axios.post(API.EDIT_CUNSTOM_TEMPLATE,options);
};

// 添加自定义模板
export const addCustomTempaleList = async (options: any) => {
  return await axios.post(API.ADD_CUNSTOM_TEMPLATE,options);
};

// 获取当前登录用户信息
export const getUserInfoCustom = async () => {
  return await axios.get(API.GET_CUNSTOM_USERINFO);
};