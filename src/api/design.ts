//我的设计相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getDesignList = async () => {
  return await axios.get(API.DESIGN_LIST);
};