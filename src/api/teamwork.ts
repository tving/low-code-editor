//协同办公的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getTeamworkist = async () => {
  return await axios.get(API.TEMPLATE_LIST);
};