//回收站请求数据
import API from "@/api/API";
import axios from "@/utils/axios";
import { getRequest } from "@/utils/fetch";
const api = process.env.VUE_APP_BASE_URL
// 获取内容列表
export const getCcontentList = async (options: any) => {
  return await axios.post(API.CONTENT_LIST, options);
};


// 新增内容
export const addCcontent = async (options: any) => {
  return await axios.post(API.ADD_CONTENT, options);
};
// 新增内容-云盘
export const addEditor = async (options: any) => {
  return await axios.post(API.ADD_EDITOR, options);
};
// 编辑内容-云盘
export const editorEditor = async (options: any) => {
  return await axios.post(API.EDIT_EDITOR, options);
};
// 获取内容-云盘
export const getEditor = async (id: any) => {
  // return await axios.get(API.GET_EDITOR+`/${id}`);
  return await getRequest(api+API.GET_EDITOR, id)
};
// 编辑内容-云盘
export const addUploads = async (options: any) => {
  return await axios.post(API.ADD_UPLOAD_FILE, options);
};
// 截图-云盘
export const obscapture = async (options: any) => {
  return await axios.post(API.obsMpcController, options);
};
// 删除内容-云盘
export const delEditor = async (options: any) => {
  return await axios.post(API.delete_editor, options);
};
// 批量删除内容-云盘
export const deleteFiles = async (options: any) => {
  return await axios.post(API.deleteFiles, options);
};


// 单个删除
export const deleteContent = async (options: any) => {
  return await axios.get(API.DELET_CONTENT + options.officeId);
};

// 批量删除
export const deleteContentList = async (options: any) => {
  return await axios.post(API.DELET_CONTENT_LIST, options.officeIdList);
};


// !!!!!!!!!!!!!!!!
// 首页--修改
export const editSpaceOffice = async (options: any) => {
  return await axios.post(API.EDIT_MODIFY, options);
};

// 分享
export const getShareOfficeList = async (options: any) => {
  return await axios.post(API.SHARE_LIST, options);
};

// 复制粘贴
export const pasteFilepost = async (options: any) => {
  return await axios.post(API.PASTE_FILE, options);
};

// 获取一条
export const getOne = async (options: any) => {
  return await axios.get(API.GET_ONE + options.officeId)
}
// 获取一条预览用
export const getOnePreview = async (options: any) => {
  return await axios.get(API.GET_ONE_PREVIEW + options.officeId)
}

export const getCurrentUserInfo = async () => {
  return await axios.get(API.CURRENT_USER_INFO);
};


// // 添加自定义组件
// export const addCustomTemplate = async (options: any) => {
//   return await axios.post(API.ADD_CUNSTOM_TEMPLATE,);
// };