//回收站请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getRecycleList = async (options: any) => {
  return await axios.post(API.RECYCLE_LIST, options);
};

export const deleteRecycleList = async (options: any) => {
  return await axios.post(API.AFOND_DELETE_FILE, options);
};

export const reductionFilePost = async (options: any) => {
  return await axios.post(API.FILE_REDUTION, options);
};