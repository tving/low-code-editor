//消息请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const getTempList = async () => {
  return await axios.get(API.NOTICE_LIST);
};