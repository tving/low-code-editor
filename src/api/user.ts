
//用户相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";
import { getRequest } from "@/utils/fetch";
const api = process.env.VUE_APP_BASE_URL
export const getCurrentUserInfo = async () => {
  // return await axios.get(API.CURRENT_USER_INFO);
  return await getRequest(api+API.CURRENT_USER_INFO,'')
};

export const getAllUser = async (options: any) => {
  return await axios.post(API.ALL_USER_LIST,options);
};

export const getUserPageList = async (options: any) => {
  return await axios.post(API.PAGE_USER_LIST,options);
};