//我的设计相关的请求数据
import API from "@/api/API";
import axios from "@/utils/axios";

export const editProcess = async (options: any) => {
    return await axios.post(API.EDIT_FILE, options);
};

// 获取已上传的图片资源
export const imageList = async (options: any) => {
    return await axios.post(API.MY_IMG_LIST, options);
};

// 获取已上传的图片资源
export const videoImageList = async (options: any) => {
    return await axios.post(API.VIDEO_IMG_LIST, options);
};