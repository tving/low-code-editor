export const getBoxWidth = (dom: string) => {
    const div: any = document.querySelector(dom)
    const width = div.clientWidth || div.offsetWidth || div.scrollWidth
    return width
}