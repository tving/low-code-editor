import { computed } from "vue";

import { storeToRefs } from "pinia";
import { useMainStore } from "@/store";

export default () => {
  const mainStore = useMainStore();
  const { canvasPercentage, canvasScale, canvasDragged } =
    storeToRefs(mainStore);
  const canvasScalePercentage = computed(
    () => Math.round(canvasScale.value * 100) + "%"
  );

  /**
   * 缩放画布百分比
   * @param command 缩放命令：放大、缩小
   */
  const scaleCanvas = (command: "+" | "-") => {
    let percentage = canvasPercentage.value;
    const step = 10;
    const max = 220;
    const min = 60;
    if (command === "+" && percentage <= max) percentage += step;
    if (command === "-" && percentage >= min) percentage -= step;

    mainStore.setCanvasPercentage(percentage);
  };

  /**
   * 设置画笔百分比
   * @param percentage 百分比（小数形式，如0.8）
   */
  const setCanvasPercentage = (percentage: number) => {
    mainStore.setCanvasPercentage(percentage);
  };

  /**
   * 重置画布尺寸和位置
   */
  const resetCanvas = () => {
    mainStore.setCanvasPercentage(90);
    if (canvasDragged) mainStore.setCanvasDragged(false);
  };

  return {
    canvasScalePercentage,
    scaleCanvas,
    setCanvasPercentage,
    resetCanvas,
  };
};
