import { useScreenStore, useSlidesStore } from '@/store'
import emitter, { EmitterEvents } from '@/utils/emitter'
import { enterFullscreen, exitFullscreen, isFullscreen } from '@/utils/fullscreen'

export default () => {
  const screenStore = useScreenStore()
  const slidesStore = useSlidesStore()

  // 进入放映状态（从当前页开始）
  const enterScreening = () => {
    // emitter.emit(EmitterEvents.SAVE_PROJECT)
    enterFullscreen()
    screenStore.setScreening(true)
    screenStore.setRenderData(false)
    screenStore.setEditCanvasLock(false)
  }

  // 进入放映状态（从第一页开始）
  const enterScreeningFromStart = () => {
    slidesStore.updateSlideIndex(0)
    enterScreening()
  }

  // 退出放映状态
  const exitScreening = () => {
    screenStore.setScreening(false)
    if (isFullscreen()) exitFullscreen()
  }

  return {
    enterScreening,
    enterScreeningFromStart,
    exitScreening,
  }
}